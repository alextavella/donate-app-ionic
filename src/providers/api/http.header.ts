import { assignIn } from 'lodash';

// Interface
export interface IHeader {
  format: Function;
}

// Json
export class JsonHeader implements IHeader {
  constructor() { }

  public format(headers: any): any
  {
    return assignIn(headers, { 'Content-Type': 'application/json' });
  }
}
