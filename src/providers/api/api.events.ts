import { Events } from "ionic-angular/util/events";
import { Injectable } from "@angular/core";

export enum ApiEventsType {
  UNAUTHORIZED = '401'
}

@Injectable()
export class ApiEvents {

  constructor(
    private events: Events
  ) { }

  public subscribe(type: ApiEventsType, handler: Function): void {
    this.events.subscribe(type, handler);
  }

  public unsubscribe(type: ApiEventsType): void {
    this.events.unsubscribe(type);
  }

  public publish(type: ApiEventsType, ...args): void {
    this.events.publish(type, args);
  }
}
