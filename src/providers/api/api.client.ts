import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import _ from "lodash";

import { Api } from './api';
import { ApiEvents, ApiEventsType } from './api.events';
import { HttpRequest, HttpMethod, IHeader, IHandler } from "./http";
import { AuthService, ITokenSaved } from '../auth/auth';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class ApiClient {

  private headers: Array<IHeader>;
  private handlers: Array<IHandler>;

  constructor(
    private api: Api,
    private apiEvents: ApiEvents
  ) {
    this.headers = [];
    this.handlers = [];
  }

  public addHeader(header: IHeader, ...args): ApiClient {
    this.headers = [];
    if (header) {
      this.headers = _.concat([header], args);
    }
    return this;
  }

  public addHandler(handler: IHandler, ...args): ApiClient {
    this.handlers = [];
    if (handler) {
      this.handlers = _.concat([handler], args);
    }
    return this;
  }

  public request(request: HttpRequest): Observable<any> {

    const doRequest = (request): Promise<any> => {

      const endpoint: any = request.endpoint;
      const params: any = request.params;
      const options: any = this.formatOptions(request.options);

      switch (request.method) {
        case HttpMethod.POST:
          return this.api.post(endpoint, params, options);
        case HttpMethod.PUT:
          return this.api.put(endpoint, params, options);
        case HttpMethod.DELETE:
          return this.api.delete(endpoint, options);
        case HttpMethod.GET:
        default:
          return this.api.get(endpoint, options);
      }
    };

    const promise = (request: HttpRequest) => {
      return new Promise((resolve, reject) => {

        doRequest(request)
          .then((resp: any) => {
            const respFormatted: any = this.formatResponse(resp);
            resolve(respFormatted);
          })
          .catch((err: any) => {

            // format type error
            this.formatError(err)
              .then(() => {

                // invoke request again
                doRequest(request)
                  .then((resp: any) => {
                    const respFormatted: any = this.formatResponse(resp);
                    resolve(respFormatted)
                  })
                  .catch((err: any) => reject(err))
                  ;
              })
              .catch(() => reject(err))
              ;
          })
          ;
      });
    };

    return Observable.fromPromise(promise(request));
  }

  private formatOptions(options?: any): any {

    if (options == undefined) {
      options = {};
    }

    if (options.headers == undefined) {
      options.headers = {};
    }

    let headers: any = _.assignIn({}, options.headers);
    this.headers.map((header: IHeader) => header.format(headers));

    return {
      headers: new HttpHeaders(headers)
    };
  }

  private formatResponse(response: any): any {
    this.handlers.map((handler: IHandler) => response = handler.format(response));
    return response;
  }

  private formatError(err: any): Promise<any> {
    return new Promise((resolve, reject) => {

      // unthorizated
      if (err.status === 401) {

        // emit event for broadcast error 401
        const dispatchEvent401 = () => {
          this.apiEvents.publish(ApiEventsType.UNAUTHORIZED);
        };

        dispatchEvent401();
        reject(err);

        const authService: AuthService = new AuthService();
        const refreshTokenData: any = this.getRefreshTokenData(authService);
        if (refreshTokenData) {

          const email: string = refreshTokenData.email;
          const refreshToken: string = refreshTokenData.refreshToken;

          // invoke refresh-token
          this.api
            .post('refresh-token', { email, refreshToken })
            .then((res: any) => {
              authService.refresh(res);
              resolve();
            })
            .catch(() => reject(err))
            ;

        } else {
          reject(err);
        }

      } else {
        reject(err);
      }
    });
  }

  private getRefreshTokenData(authService: AuthService): any {
    const token: ITokenSaved = authService.load();
    if (authService.isValid(token)) {
      const email: string = token.email;
      const refreshToken: string = token.refreshToken;
      return { email, refreshToken };
    }
    return null;
  }
}
