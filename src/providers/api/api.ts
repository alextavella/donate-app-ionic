import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from'../../environments/environment';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {

  constructor(
    private http: HttpClient
  ) { }

  public get(endpoint: string, options?: any): Promise<any> {
    const url = this.updateUrl(endpoint);
    return this.http.get(url, options).toPromise();
  }

  public post(endpoint: string, body: any, options?: any): Promise<any> {
    const url = this.updateUrl(endpoint);
    return this.http.post(url, body, options).toPromise();
  }

  public put(endpoint: string, body: any, options?: any): Promise<any> {
    const url = this.updateUrl(endpoint);
    return this.http.put(url, body, options).toPromise();
  }

  public delete(endpoint: string, options?: any): Promise<any> {
    const url = this.updateUrl(endpoint);
    return this.http.delete(url, options).toPromise();
  }

  public patch(endpoint: string, body: any, options?: any): Promise<any> {
    const url = this.updateUrl(endpoint);
    return this.http.put(url, body, options).toPromise();
  }

  private updateUrl(req: string): string {
    return `${environment.prod}/${req}`;
  }
}
