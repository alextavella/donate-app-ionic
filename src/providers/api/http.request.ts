export class HttpRequest {
  constructor(
    public method: HttpMethod,
    public endpoint: string,
    public params?: any,
    public options?: any
  ) { }
}

export enum HttpMethod {
  GET,
  POST,
  PUT,
  DELETE
}
