import { map } from 'lodash';
import { IHandler } from '../api/http.handler';
import { IAddressUser, IAddressCep } from './address.model';

export class AddressCepHandler implements IHandler {
  constructor() { }

  public format(response: any): IAddressCep {
    return {
      street: response.street,
      cep: response.cep,
      neighborhood: response.neighborhood,
      city: response.city,
      state: response.state,
    };
  }
}

export class AddressHandler implements IHandler {
  constructor() { }

  public format(response: any): IAddressUser {
    if (response) {
      return {
        id: response.id,
        alias: response.alias,
        street: response.street,
        cep: response.cep,
        number: response.number,
        complement: response.complement,
        neighborhood: response.neighborhood,
        city: response.city,
        state: response.state,
        country: {
          short_name: response.country.short_name,
          long_name: response.country.long_name
        },
        location: {
          lat: response.location.lat,
          lng: response.location.lng,
        }
      };
    }
    return null;
  }
}

export class AddressListHandler implements IHandler {
  constructor() { }

  public format(response: any): Array<IAddressUser> {
    return map(response.addresses || [], new AddressHandler().format);
  }
}
