import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiClient } from './../api/api.client';
import { AuthHeader, JsonHeader } from '../headers';
import { HttpRequest, HttpMethod } from '../providers';
import { AddressHandler, AddressCepHandler, AddressListHandler } from './address.handler';

@Injectable()
export class Address {

  constructor(
    private apiClient: ApiClient
  ) { }

  public searchAddressByCep(cep: string) {
    return this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new AddressCepHandler())
      .request(new HttpRequest(HttpMethod.GET, 'address/cep/' + cep))
      .share()
      ;
  }

  public load(): Observable<any> {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(new AddressListHandler())
      .request(new HttpRequest(HttpMethod.GET, 'address'))
      .share()
      ;
  }

  public register(address: any) {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(null)
      .request(new HttpRequest(HttpMethod.POST, 'address', address))
      .share()
      ;
  }
}
