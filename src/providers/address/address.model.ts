export class IAddressCep {
  street: string;
  cep: string;
  neighborhood: string;
  city: string;
  state: string;
}

export class IAddressUser
{
  id: string;
  alias: string;
  cep: string;
  street: string;
  number: number;
  complement: string;
  neighborhood: string;
  city: string;
  state: string;
  country: ICountry;
  location: ILocation;
}

export class ICountry
{
  short_name: string;
  long_name: string;
}

export class ILocation
{
  lng: number;
  lat: number;
}
