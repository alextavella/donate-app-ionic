import { IAddressUser } from './../address/address.model';
import { IPhone } from '../phone/phone.model';
import { ICategory } from '../category/category.model';

export class IProduct
{
  id: string;
  name: string;
  description: string;
  photo?: string;
  category: ICategory;
  address: IAddressUser;
  phone: IPhone;
}
