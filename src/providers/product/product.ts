import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { JsonHeader } from '../api/http.header';
import { AuthHeader } from '../auth/auth.header';
import { ApiClient } from '../api/api.client';
import { HttpRequest, HttpMethod } from '../providers';
import { ProductHandler, ProductListHandler } from './product.handler';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Product {

  constructor(
    private apiClient: ApiClient
  ) { }

  public register(product: any) {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(null)
      .request(new HttpRequest(HttpMethod.POST, 'product', product))
      .share()
      ;
  }

  public edit(product: any) {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(new ProductHandler())
      .request(new HttpRequest(HttpMethod.PUT, 'product', product))
      .share()
      ;
  }

  public list() {
    return this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new ProductListHandler())
      .request(new HttpRequest(HttpMethod.GET, 'product'))
      .share()
      ;
  }

  public get(id: string) {
    return this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new ProductHandler())
      .request(new HttpRequest(HttpMethod.GET, `product/${id}`))
      .share()
      ;
  }

  public remove(id: string) {
    return this.apiClient
      .addHeader(new AuthHeader())
      .addHandler(null)
      .request(new HttpRequest(HttpMethod.DELETE, `product/${id}`))
      .share()
      ;
  }
}
