import _ from 'lodash';
import { IHandler } from '../api/http.handler';
import { IProduct } from './product.model';
import { AddressHandler } from './../address/address.handler';
import { CategoryHandler } from '../category/category.handler';
import { PhoneHandler } from '../phone/phone.handler';
import { PhotoHandler } from '../../services/photo/photo.service';

export class ProductHandler implements IHandler {
  constructor() { }

  public format(response: any): IProduct {
    return {
      id: response.id,
      name: response.name,
      description: response.description,
      photo: new PhotoHandler().format(response.photo),
      address: new AddressHandler().format(response.address),
      category: new CategoryHandler().format(response.category),
      phone: new PhoneHandler().format(response.phone)
    };
  }
}

export class ProductListHandler implements IHandler {
  constructor() { }

  public format(response: any): Array<IProduct> {
    return _.map(response.products || [], new ProductHandler().format);
  }
}
