import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { JsonHeader } from '../api/http.header';
import { AuthHeader } from '../auth/auth.header';
import { ApiClient } from '../api/api.client';
import { HttpRequest, HttpMethod } from '../providers';
import { PhoneListHandler, PhoneHandler } from './phone.handler';

/*
  Generated class for the PhoneProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Phone {

  constructor(
    private apiClient: ApiClient
  ) { }

  public register(phone: any) {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(new PhoneHandler())
      .request(new HttpRequest(HttpMethod.POST, 'phone', phone))
      .share()
      ;
  }

  public load() {
    return this.apiClient
      .addHeader(new JsonHeader(), new AuthHeader())
      .addHandler(new PhoneListHandler())
      .request(new HttpRequest(HttpMethod.GET, 'phone'))
      .share()
      ;
  }
}
