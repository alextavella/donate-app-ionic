import { map } from 'lodash';
import { IHandler } from '../api/http.handler';
import { IPhone } from './phone.model';

export class PhoneHandler implements IHandler {
  constructor() { }

  public format(response: any): IPhone {
    if (response) {
      return {
        id: response.id,
        name: response.name,
        number: response.number
      };
    }
    return null;
  }
}

export class PhoneListHandler implements IHandler {
  constructor() { }

  public format(response: any): IPhone {
    return map(response.phones || [], new PhoneHandler().format);
  }
}
