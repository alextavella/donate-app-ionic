
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { ApiClient } from './../api/api.client';
import { JsonHeader } from '../headers';
import { HttpRequest, HttpMethod } from '../providers';
import { ITokenSaved } from '../auth/auth.model';
import { AuthHandler } from './../auth/auth.handler';


@Injectable()
export class User {

  private KEY: string = "USER_LOGON";

  constructor(
    private apiClient: ApiClient
  ) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  public login(accountInfo: any): Observable<any> {

    const keepConnected = accountInfo.keepConnected;
    const seq = this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new AuthHandler(keepConnected))
      .request(new HttpRequest(HttpMethod.POST, 'login', accountInfo))
      .share()
      ;

    seq
      .subscribe((resp: ITokenSaved) => {
        this._loggedIn(resp);
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  public signup(accountInfo: any) {

    const seq = this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new AuthHandler())
      .request(new HttpRequest(HttpMethod.POST, 'register', accountInfo))
      .share()
      ;

    seq
      .subscribe((res: ITokenSaved) => {
        this._loggedIn(res);
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  public isLogon(): boolean {
    return this.getUser() !== null;
  }

  /**
   * Log the user out, which forgets the session
   */
  public logout() {
    localStorage.removeItem(this.KEY);
  }

  /**
   * Process a login/signup response to store user data
   */
  private _loggedIn(resp: any): void {
    if (resp) {
      localStorage.setItem(this.KEY, JSON.stringify(resp));
    }
  }

  private getUser(): any {
    const saved: any = localStorage.getItem(this.KEY);
    return saved ? JSON.parse(saved) : null;
  }
}

