export interface IAuth
{
  token: IToken;
  user: IUser;
  reconnect: boolean;
}

export interface IToken
{
  accessToken: string;
  expiresIn: Date;
  refreshToken: string;
  tokenType: string;
}

export interface IUser
{
  email: string;
  id: string;
}

export interface ITokenSaved
{
  accessToken: string;
  refreshToken: string;
  email: string;
}
