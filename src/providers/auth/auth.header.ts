import { assignIn } from 'lodash';

import { IHeader } from '../api/http.header';
import { AuthService } from './auth.service';

export class AuthHeader implements IHeader
{
  private authenticateService = new AuthService();

  constructor() { }

  public format(headers: any): any
  {
    const token = this.authenticateService.load().accessToken;
    return assignIn(headers, { Authorization: `Bearer ${token}` });
  }
}
