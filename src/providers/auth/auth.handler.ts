import { AuthService } from '../auth/auth.service';
import { IAuth, IToken, IUser } from './auth.model';
import { IHandler } from '../api/http.handler';

export class AuthHandler implements IHandler
{
  private authService;

  constructor(
    private reconnect: boolean = false)
  {
    this.authService = new AuthService();
  }

  public format(response): any
  {
    const auth: IAuth = {
      token: this.formatToModel<IToken>(response.token),
      user: this.formatToModel<IUser>(response.user),
      reconnect: this.reconnect
    };

    this.authService.save(auth);
    return response.user;
  }

  private formatToModel<T>(response: any): T
  {
    return <T>response;
  }
}
