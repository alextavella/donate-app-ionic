import _ from 'lodash';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ITokenSaved, IAuth, IToken } from './auth.model';

export class AuthService {

  private KEY: string = "token";

  public save(auth: IAuth): void {
    const accessToken: string = auth.token.accessToken;
    const refreshToken: string = auth.reconnect ? auth.token.refreshToken : '';
    const email: string = auth.user.email;
    this.saveCookie({ accessToken, refreshToken, email });
  }

  public refresh(refresh: IToken): void {
    const accessToken: string = refresh.accessToken;
    const refreshToken: string = refresh.refreshToken;
    const update: ITokenSaved = _.assignIn({}, this.load(), { accessToken, refreshToken });
    this.saveCookie(update);
  }

  public load(): ITokenSaved {
    const json: string = Cookie.get(this.KEY);
    return json ? JSON.parse(json) : { accessToken: '', refreshToken: '', email: '' };
  }

  public isValid(token: ITokenSaved): boolean {
    return token.refreshToken && token.refreshToken.length > 0;
  }

  private saveCookie(token: ITokenSaved): void {
    console.log('accessToken', token.accessToken);
    console.log('refreshToken', token.refreshToken);
    Cookie.set(this.KEY, JSON.stringify(token));
  }
}
