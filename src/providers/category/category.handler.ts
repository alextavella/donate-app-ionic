import { map } from 'lodash';
import { IHandler } from '../api/http.handler';
import { ICategory } from './category.model';

export class CategoryHandler implements IHandler {
  constructor() { }

  public format(response: any): ICategory {
    if (response) {
      return {
        id: response.id,
        name: response.name,
        code: response.code,
        image: {
          name: response.image.name,
          url: response.image.url,
          base64: response.image.base64
        }
      };
    }
    return null;
  }
}

export class CategoryListHandler implements IHandler {
  constructor() { }

  public format(response: any): Array<ICategory> {
    const formatCategory = (response) => new CategoryHandler().format(response);
    return map(response.categories || [], formatCategory);
  }
}
