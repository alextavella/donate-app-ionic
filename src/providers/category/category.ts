import { Injectable } from '@angular/core';
import { ApiClient } from '../api/api.client';
import { HttpRequest, HttpMethod } from '../providers';
import { CategoryListHandler } from './category.handler';
import { JsonHeader } from '../api/http';

/*
  Generated class for the CategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Category {

  constructor(
    private apiClient: ApiClient
  ) { }

  public load() {
    return this.apiClient
      .addHeader(new JsonHeader())
      .addHandler(new CategoryListHandler())
      .request(new HttpRequest(HttpMethod.GET, 'category'))
      .share();
  }
}
