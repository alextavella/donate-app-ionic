export class ICategory {
  id: string;
  name: string;
  code: number;
  image: ICategoryImage;
}

export class ICategoryImage {
  name?: string;
  url: string;
  base64?: any;
}
