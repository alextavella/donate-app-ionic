import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string, keepConnected: boolean } = {
    email: 'test@example.com',
    password: '123456',
    keepConnected: true
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

    if (this.user.isLogon()) {
      this.navCtrl.push(MainPage);
    }
  }

  // Attempt to login in through our User service
  public doLogin() {
    this.user
      .login(this.account)
      .subscribe(
      (resp) => this.handlerLogin(resp)
      , (err) => this.handlerError(err)
      );
  }

  private handlerLogin(resp: any): void {
    this.navCtrl.push(MainPage);
  }

  private handlerError(err: any): void {
    this.toastCtrl
      .create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      })
      .present();
  }
}
