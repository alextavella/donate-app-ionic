import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { firstName: string, lastName: string, email: string, password: string } = {
    firstName: 'Alex',
    lastName: 'Tavella',
    email: 'alextavella@gmail.com',
    password: 'asdasd'
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(
    private navCtrl: NavController,
    private user: User,
    private toastCtrl: ToastController,
    private translateService: TranslateService) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  public doSignup() {
    // Attempt to login in through our User service
    this.user
      .signup(this.account)
      .subscribe((resp) => this.handlerSignup(resp)
      , (err) => this.handlerError(err)
      );
  }

  private handlerSignup(resp: any): void {
    this.navCtrl.push(MainPage);
  }

  private handlerError(err: any): void {
    this.toastCtrl
      .create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      })
      .present();
  }
}
