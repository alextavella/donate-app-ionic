import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Phone } from '../../providers/phone/phone'
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';

/**
 * Generated class for the PhoneRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phone-register',
  templateUrl: 'phone-register.html',
})
export class PhoneRegisterPage {

  private formGroup: FormGroup;
  // Our translated text strings
  private errorMessage: string;

  constructor(public navCtrl: NavController,
    public phone: Phone,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      number: ['', Validators.required]
    });

    this.translateService.get('REGISTER_ERROR').subscribe((value) => {
      this.errorMessage = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneRegisterPage');
  }

  register() {
    this.phone
      .register(this.formGroup.value)
      .subscribe(resp => {
        this.navCtrl.pop();
      }, err => {
        // Unable to register
        let toast = this.toastCtrl.create({
          message: this.errorMessage,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
  }
}
