import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneRegisterPage } from './phone-register';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PhoneRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneRegisterPage),
    TranslateModule.forChild()
  ],
})
export class PhoneRegisterPageModule {}
