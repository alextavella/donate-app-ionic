import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Address } from '../../providers/address/address'
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { AddressRegisterPage, PhoneSelectorPage } from '../pages';
import { RegistrationService } from '../../services'
import { IAddressUser } from '../../providers/address/address.model';
import { IProduct } from '../../providers/product/product.model';

@IonicPage()
@Component({
  selector: 'page-address-selector',
  templateUrl: 'address-selector.html',
})
export class AddressSelectorPage {
  private formGroup: FormGroup;
  private errorMessage: string;
  private product: IProduct;
  private addresses: Array<IAddressUser> = [];

  constructor(public navCtrl: NavController,
    public address: Address,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService) {
    this.product = registrationService.load();

    this.formGroup = this.formBuilder.group({
      address: ['', Validators.required]
    });

    this.translateService.get('REGISTER_ERROR').subscribe((value) => {
      this.errorMessage = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressSelectorPage');
  }

  ionViewWillEnter() {
    //Load addresses on select field
    this.address
      .load()
      .subscribe(resp => {
        console.log('addresses', resp);
        this.addresses = resp;
      }, err => {
        console.log(err);
      });
  }

  register() {
    this.navCtrl.push(AddressRegisterPage)
  }

  next() {
    this.registrationService.update(this.formGroup.value);
    this.navCtrl.push(PhoneSelectorPage);
  }
}
