import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddressSelectorPage } from './address-selector';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddressSelectorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddressSelectorPage),
    TranslateModule.forChild()
  ],
})
export class AddressSelectorPageModule {}
