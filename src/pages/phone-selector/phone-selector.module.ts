import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneSelectorPage } from './phone-selector';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PhoneSelectorPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneSelectorPage),
    TranslateModule.forChild()
  ],
})
export class PhoneSelectorPageModule {}
