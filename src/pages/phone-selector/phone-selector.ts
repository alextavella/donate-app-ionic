import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Phone } from '../../providers/phone/phone'
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { PhoneRegisterPage, ProductDetailPage } from '../pages';
import { RegistrationService } from '../../services'
import { IPhone } from '../../providers/phone/phone.model';
import { IProduct } from '../../providers/product/product.model';

/**
 * Generated class for the PhoneSelectorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phone-selector',
  templateUrl: 'phone-selector.html',
})
export class PhoneSelectorPage {
  private formGroup: FormGroup;
  private errorMessage: string;
  private product: IProduct;
  private phones: Array<IPhone> = [];

  constructor(public navCtrl: NavController,
    public phone: Phone,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService) {
    this.product = registrationService.load();

    this.formGroup = this.formBuilder.group({
      phone: ['', Validators.required]
    });

    this.translateService.get('REGISTER_ERROR').subscribe((value) => {
      this.errorMessage = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneSelectorPage');
  }

  ionViewWillEnter() {
    //Load phones on select field
    this.phone
      .load()
      .subscribe(resp => {
        console.log('phones', resp);
        this.phones = resp;
      }, err => {
        // Unable to get address by inputed cep
        console.log(err);
      });
  }

  register() {
    this.navCtrl.push(PhoneRegisterPage)
  }

  next() {
    this.registrationService.update(this.formGroup.value);
    this.navCtrl.push(ProductDetailPage, {
      product: this.registrationService.load(),
      isRegistration: true
    });
  }
}
