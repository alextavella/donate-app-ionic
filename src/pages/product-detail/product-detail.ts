import { PhotoHandler } from './../../services/photo/photo.service';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, NavParams } from 'ionic-angular';
import { Product } from '../../providers/product/product'
import { ProductListPage } from '../pages';
import { RegistrationService } from '../../services/product/registration.service';

@IonicPage()
@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetailPage {

  private errorMessage: string;
  private productInfo: any;
  private photo: string;
  private isRegistration: Boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private product: Product,
    private registrationService: RegistrationService) {

    this.productInfo = this.registrationService.load();
    this.photo = new PhotoHandler().format(this.productInfo.photo);
    this.isRegistration = this.navParams.get('isRegistration') || false;

    this.translateService.get('REGISTER_ERROR').subscribe((value) => {
      this.errorMessage = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailPage');
  }

  register() {

    const product = {
      name: this.productInfo.name,
      description: this.productInfo.description,
      category: this.productInfo.category.id,
      address: this.productInfo.address.id,
      phone: this.productInfo.phone.id,
      photo: this.productInfo.photo
    };

    this.product
      .register(product)
      .subscribe(() => {
        this.registrationService.clear();
        this.navCtrl.setRoot(ProductListPage);
      }, err => {
        console.log(err);
      })
  }
}
