// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'TutorialPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'ProductListPage';
export const LoginPage = "LoginPage";

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';

// The page to register address info
export const AddressSelectorPage = 'AddressSelectorPage';
export const AddressRegisterPage = 'AddressRegisterPage';

// The page to register phone info
export const PhoneSelectorPage = 'PhoneSelectorPage';
export const PhoneRegisterPage = 'PhoneRegisterPage';

// The page to register category info
export const CategoryRegisterPage = 'CategoryRegisterPage';

export const ProductRegisterPage = 'ProductRegisterPage';
export const ProductDetailPage = 'ProductDetailPage';

// The page to select category
export const CategoryPage = 'CategoryPage';

// The page list product
export const ProductListPage = 'ProductListPage';
