import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddressRegisterPage } from './address-register';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddressRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(AddressRegisterPage),
    TranslateModule.forChild()
  ],
})
export class AddressRegisterPageModule {}
