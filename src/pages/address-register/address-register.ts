import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { debounce } from 'lodash';
import { Address } from '../../providers/address/address'
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';

/**
 * Generated class for the AddressRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-address-register',
  templateUrl: 'address-register.html',
})
export class AddressRegisterPage {
  private formGroup: FormGroup;
  private errorMessage: string;
  
  constructor(public navCtrl: NavController,
    public address: Address,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private formBuilder: FormBuilder) {
      this.formGroup = this.formBuilder.group({
        alias: ['', Validators.required],
        cep: ['', Validators.required],
        street: ['', Validators.required],
        number: ['', Validators.required],
        neighborhood: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required]
      });
      
      this.formGroup.controls['cep'].valueChanges
        .subscribe(debounce(this.searchAddressByCep.bind(this), 1000));
      
      this.translateService.get('REGISTER_ERROR').subscribe((value) => {
        this.errorMessage = value;
      })
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressRegisterPage');
  }

  searchAddressByCep() {
    this.address.searchAddressByCep(this.formGroup.controls['cep'].value)
      .subscribe(resp => {
        const { street, neighborhood, city, state } = resp;

        if (street) this.formGroup.controls['street'].setValue(street);
        if (neighborhood) this.formGroup.controls['neighborhood'].setValue(neighborhood);
        if (city) this.formGroup.controls['city'].setValue(city);
        if (state) this.formGroup.controls['state'].setValue(state);
      }, err => {
        // Unable to get address by inputed cep
        console.log(err);
      });
  }

  register() {
    this.address.register(this.formGroup.value).subscribe(resp => {
      this.navCtrl.pop();
    }, err => {
      // Unable to register
      let toast = this.toastCtrl.create({
        message: this.errorMessage,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    })
  }
}
