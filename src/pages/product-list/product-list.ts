import { IProduct } from './../../providers/product/product.model';
import { Product } from './../../providers/product/product';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductDetailPage, CategoryPage } from '../pages';
import { remove } from 'lodash';

/**
 * Generated class for the ProductListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {

  public products: Array<IProduct> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private product: Product
  ) { }

  ionViewWillEnter() {
    console.log('ionViewDidLoad ProductListPage');

    this.product
      .list()
      .subscribe((resp: Array<IProduct>) => {
        console.log('products', resp);
        this.products = resp;
      })
      ;
  }

  add() {
    this.navCtrl.push(CategoryPage);
  }

  details(product: IProduct) {
    this.navCtrl.push(ProductDetailPage, {
      product,
      isRegistration: false
    })
  }

  remove(product: IProduct) {
    this.product
      .remove(product.id)
      .subscribe(() => {
        remove(this.products, { id: product.id })
      })
      ;
  }
}
