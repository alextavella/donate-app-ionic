import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductRegisterPage } from './product-register';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ProductRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductRegisterPage),
    TranslateModule.forChild()
  ],
  exports: [
    ProductRegisterPage
  ]
})
export class ProductPhotoPageModule { }
