import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { IProduct } from '../../providers/product/product.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { RegistrationService } from '../../services/index';
import { AddressSelectorPage } from '../pages';
import { PhotoService } from './../../services/photo/photo.service';

@IonicPage()
@Component({
  selector: 'page-product-register',
  templateUrl: 'product-register.html',
})
export class ProductRegisterPage {
  private product: IProduct;
  private formGroup: FormGroup;
  private errorMessage: string;
  private photoRandom: any;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private registrationService: RegistrationService,
    private formBuilder: FormBuilder,
    private photoService: PhotoService) {

    this.product = this.registrationService.load();

    this.formGroup = this.formBuilder.group({
      name: [this.product.name, Validators.required],
      description: [this.product.description, Validators.required],
      photo: [this.product.photo, Validators.required]
    });

    this.translateService.get('REGISTER_ERROR').subscribe((value) => {
      this.errorMessage = value;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductRegisterPage');
  }

  selectPhoto() {
    this.photoRandom = this.photoService.getRandom();
    this.product.photo = this.photoRandom.url;
  }

  next() {
    let product = this.formGroup.value;
    if (this.photoRandom) {
      product.photo = this.photoRandom.name;
    }
    this.registrationService.update(product);
    this.navCtrl.push(AddressSelectorPage);
  }
}
