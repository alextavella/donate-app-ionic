import { ProductRegisterPage } from '../pages';
import { ICategory } from './../../providers/category/category.model';
import { Category } from './../../providers/category/category';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrationService } from '../../services/product/registration.service';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  public categories: Array<ICategory> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private category: Category,
    private registrationService: RegistrationService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }

  ionViewWillEnter() {
    this.category
      .load()
      .subscribe((resp: Array<ICategory>) => {
        console.log('categories', resp);
        this.categories = resp;
      })
      ;
  }

  next(category: ICategory) {
    this.registrationService.update({ category: category });
    this.navCtrl.push(ProductRegisterPage);
  }
}
