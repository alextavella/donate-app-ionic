import { assignIn } from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class RegistrationService {

  private KEY: string = 'PRODUCT_SAVE';

  constructor() {
    this.update({});
  }

  public update(product): any {
    const save = assignIn({}, this.load(), product);
    localStorage.setItem(this.KEY, JSON.stringify(save));
    console.log('Informações do produto:', save);
  }

  public load(): any {
    const saved: any = localStorage.getItem(this.KEY);
    return saved ? JSON.parse(saved) : { photo: 'http://placehold.it/216x216' };
  }

  public clear(): void {
    localStorage.removeItem(this.KEY);
  }
}
