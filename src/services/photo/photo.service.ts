import { Injectable } from '@angular/core';
import { IPhoto } from '../../providers/photo/photo.model';

@Injectable()
export class PhotoService {

  public photos: Array<IPhoto> = [
    { name: 'camera.jpg' },
    { name: 'game.jpg' },
    { name: 'geladeira.jpg' },
    { name: 'iphone.jpg' },
    { name: 'liquidificador.jpg' },
    { name: 'sofa.jpg' },
    { name: 'tv.jpg' }
  ];

  constructor() { }

  public get(index: number): IPhoto {
    let photo: IPhoto = this.photos[index];
    photo.url = this.formatUrlImage(photo.name);
    return photo;
  }

  public getRandom(): IPhoto {
    const random = Math.floor(Math.random() * this.photos.length);
    return this.get(random);
  }

  public formatUrlImage(name: string): string {
    return new PhotoHandler().format(name);
  }
}

export class PhotoHandler {

  private PLACEHOLDER: string = 'http://placehold.it/126x126';

  constructor() { }

  public format(name: string): string {
    if (!name) return this.PLACEHOLDER;
    return `assets/img/photos/${name}`;
  }
}
